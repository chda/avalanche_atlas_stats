# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 09:51:45 2019

@author: decne
"""

import avaconnect 
import shapely
from shapely import wkt, wkb
def db_query():
    print('loading Database, please enter PWD')
    #####Reading WKT (Text) Geometries from Postgis

    list=avaconnect.query(  'SELECT st_length('
                                'path_ln3d'
                            ') '
                            'as path_ln3d from event_full where event_id = 30')
    
    # Load first avalanche path, converts WKT to shapely format
    
    ava_paths = []

    paths=list['path_ln3d']
    ava_path=shapely.wkt.loads(paths[0])
    xyzdump=ava_path.coords[:]
    return xyzdump