#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 09:07:11 2019

@author: lawinenforschung
Script reading in the profile and forest from .txt
"""


import numpy as np
from scipy import interpolate
import avaconnect


def regrid(resolution, profile):
    '''
    Interpolation of Points along the Profile with a certain resolution
    '''
    x = np.arange(0, profile[-1, 0], resolution)
    #inter = interpolate.interp1d(profile[: , 0], profile[: , 1])
    inter = interpolate.interp1d(profile[: , 0], profile[: , 1], axis=0, fill_value="extrapolate")
    z_new = inter(x)
    profile_resolution = np.column_stack((x, z_new))
    return profile_resolution

# Reading data:
def read_path(profile_filename):

    profile = np.genfromtxt(profile_filename, skip_header=1)  # [X, Z]
    profile_res = regrid(1, profile)        
    return profile_res

def read_db_path(profile_id):
    querystr=   'SELECT distinct profile_from_peak(' \
                    'add_interpolate_point(' \
                        'add_interpolate_point(' \
                            'resampled_ln_z(' \
                                'geom_path_ln' \
                            '), ' \
                            'geom_origin_pt3d' \
                        '),' \
                        'geom_runout_pt3d' \
                    ')' \
                ') as dz from event_full WHERE path_id = {}'.format(profile_id)
    profile = np.array(avaconnect.get_line(querystr, 'dz'))
    profile_res = regrid(1, profile)        
    return profile_res

def read_db_available_paths():
    querystr = "select distinct path_id from event_full order by path_id"
    data = avaconnect.query(querystr)
    return data
    

def read_forest(forest_filename):
    '''
    [ID, X1, X2, NrTree, DbhMean, DbhStd]
    When only one forest is stored in the data it will be read in as a 1D array.
    To have always the same structure we make a 2D array out of it.
    '''
    
    forest = np.genfromtxt(forest_filename, skip_header=1)
    if len(forest.shape) == 1:
        forest = np.array([forest])    
    return forest



def read_db_events(path_id, minimum_avalanche_size =3):
    minimum_ava_size =3
    events = np.array(avaconnect.query( "select ("
                                            "st_dumppoints("
                                                "profile_from_peak("
                                                    "geom_event_pt, "
                                                    "add_interpolate_point("
                                                        "add_interpolate_point("
                                                            "resampled_ln_z("
                                                                "geom_path_ln"
                                                            "),"
                                                            "geom_origin_pt3d"
                                                        "),"
                                                        "geom_runout_pt3d"
                                                    ")"
                                                ")"
                                            ")"
                                        ").geom, "
                                        "avalanche_size, "
                                        "timestamp from event_full where path_id = {}".format(path_id)))
    return  events

def read_db_ava_data(path_id):
    events = avaconnect.query("select path_id, avalanche_size, path_name, st_astext(st_startpoint(profile_from_peak("
                              "geom_origin_pt, add_interpolate_point(add_interpolate_point(resampled_ln_z(geom_path_ln),geom_origin_pt3d),geom_runout_pt3d)))) as origin, st_astext(st_startpoint("
                              "profile_from_peak(geom_transit_pt, add_interpolate_point(add_interpolate_point(resampled_ln_z(geom_path_ln),geom_origin_pt3d),geom_runout_pt3d)))) as transit, st_astext("
                              "st_startpoint(profile_from_peak(geom_runout_pt, add_interpolate_point(add_interpolate_point(resampled_ln_z(geom_path_ln),geom_origin_pt3d),geom_runout_pt3d)))) as "
                              "runout  from event_full where path_id = {}".format(path_id))
    return  events

def read_infra(infra_filename):
    
    infra = np.genfromtxt(infra_filename, skip_header=1)
    # [ID, X1, X2]
    
    return infra