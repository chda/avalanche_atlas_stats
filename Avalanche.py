#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This is where the avalanche stopping criteria lives.
Responsible - Christopher & Michael
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick

def forest_idx(x, forest):
    '''
    Reads out the Position of the Forest

    '''
    forest_value = np.zeros_like(x)
    for line in forest:
        idx1 = np.where(x == line[1])[0]
        idx2 = np.where(x == min(len(x),(line[2]-1)))[0]
        forest_type = np.where(x == line[3])[0] # 1 = bushes, 2 = mixed forest, 3 = evergreen

        forest_density =  line[4]#np.where(x == line[4])[0]
        if forest_type == 1:
            forest_value[idx1[0]:idx2[0]] = .2
        elif forest_type ==2:
            forest_value[idx1[0]:idx2[0]] = .05 + forest_density * .75
        elif forest_type ==3:
            forest_value[idx1[0]:idx2[0]] = .25 + forest_density * .75
        else:
            print("ohhh dang there is no forest info")
        if forest_density <= .2:
            forest_value[idx1[0]:idx2[0]] = .2
    return forest_value
Snowmabn


Snowman


def energy_line(x, z, forest , resolution, alpha, alpha_sigma, alpha_forest,start_pt=0):
    forest_value = forest_idx(x, forest)
    elh = np.zeros_like(z)
    elh_plus = np.zeros_like(z)
    elh_minus = np.zeros_like(z)

    for i in range(start_pt,len(x)-1):

        max_added_friction = (alpha_forest * forest_value[i])
        alpha_calc = alpha + max(0, -elh[i] * (max_added_friction / 45) + max_added_friction)#/ elh[i] elh/15 -> no more forest effect at 150 m elh
        elh[i+1] =  min(250, max(0, elh[i]  +(z[i]-z[i+1])- resolution * np.tan(np.deg2rad(alpha_calc))))

        alpha_calc = alpha + max(0, -elh_plus[i] * (max_added_friction / 45) + max_added_friction)
        elh_plus[i + 1] = min(250, max(0, elh_plus[i] + (z[i] - z[i + 1]) - resolution * np.tan(np.deg2rad(alpha_calc + alpha_sigma))))

        alpha_calc = alpha + max(0, -elh_minus[i] * (max_added_friction / 45) + max_added_friction)
        elh_minus[i + 1] = min(250, max(0, elh_minus[i] + (z[i] - z[i + 1]) - resolution * np.tan(np.deg2rad(alpha_calc - alpha_sigma))))

    # below function picks out the longest energy line in the case there are more than one line drawn.
    elh_processed = getLongestSeq(elh)
    elh_plus_processed = getLongestSeq(elh_plus)
    elh_minus_processed = getLongestSeq(elh_minus)

    return elh_processed, elh_plus_processed, elh_minus_processed



def getLongestSeq(a):
    """
    This function will pick out the length of the longest energy line and the start index of the line
    This is useful so we only consider one energy line for our forest indicator.
    Problems of many energy lines come from not smooth terrain that may make many short (few meter) avalanches along the path.
    """
    n = len(a)
    maxIdx = 0
    maxLen = 0
    currLen = 0
    currIdx = 0

    for k in range(n):
        if a[k] > 0:
            currLen +=1
            # New sequence, store
            # beginning index.
            if currLen == 1:
                currIdx = k
        else:
            if currLen > maxLen:
                maxLen = currLen
                maxIdx = currIdx
            currLen = 0
    elh = np.zeros_like(a) # first initialize the elh with zeros

    if maxLen > 0:
        for cell_no in range (maxIdx, maxIdx+maxLen,1):
            elh[cell_no] = a[cell_no] + elh[cell_no]
    else:
        print("No avalanche detected, slope not steep enough.")

    return elh


def Avalanche(profile, forest, start_pt=0):

    # Initialisation of Variables    
    alpha = 25
    alpha_forest = 10  # Increase of alpha angle in the forest
    alpha_sigma = 1.5  # standard deviation on alpha used for both with and without forest

    # impact of 100 kPa will break well developed forest (Rapin 202) https://www.researchgate.net/profile/Francois_Rapin/publication/267562272_A_new_scale_for_avalanche_intensity/links/59ed9a88a6fdccef8b0dd7a4/A-new-scale-for-avalanche-intensity.pdf
    # max speed of avalanche has been measured by radar 23m/s (Gubler 1987, http://hydrologie.org/redbooks/a162/iahs_162_0405.pdf)
    # Therefore at about 200 kg/m^3 density going full speed @ 23m/s we have no more forest.
    x = profile[:, 0]
    z = profile[:, 1]    
    resolution = x[1] - x[0]
    
    elh, elh_plus, elh_minus= energy_line(x, z, forest, resolution, alpha, alpha_sigma, 0, start_pt) # no forest due to 0
    elh_forest, elh_forest_plus, elh_forest_minus = energy_line(x, z, forest, resolution, alpha, alpha_sigma ,alpha_forest, start_pt) # forest
    #### here we make the forest indicator
    forest_indicator = np.zeros(len(z))
    forest_indicator_minus = np.zeros(len(z)) # calculated with minus sigma_alpha (1.5 deg) is alphas standard deviation
    forest_indicator_plus = np.zeros(len(z))  # calculated with plus sigma_alpha
    # % diff is used for forest indicator
    for i in range(start_pt, len(x)):
        if elh[i] > 0:
            forest_indicator[i] = (elh[i] - elh_forest[i])/(elh[i] ) * 100
        elif elh[i] <= 0:
            forest_indicator[i] = 100
        if elh_minus[i] > 0:
            forest_indicator_minus[i] = (elh_minus[i] - elh_forest_minus[i])/(elh_minus[i] ) * 100
        elif elh_minus[i] <= 0:
            forest_indicator_minus[i] = 100


        if elh_plus[i] > 0:
            forest_indicator_plus[i] = (elh_plus[i] - elh_forest_plus[i]) / (elh_plus[i] ) * 100
        elif elh_plus[i]<= 0:
            forest_indicator_plus[i] = 100

    ELH = np.column_stack((elh, elh_plus, elh_minus))
    ELH_FOREST = np.column_stack((elh_forest, elh_forest_plus, elh_forest_minus))
    FOREST_INDICATOR = np.column_stack((forest_indicator, forest_indicator_plus, forest_indicator_minus))

    return ELH, ELH_FOREST, FOREST_INDICATOR
