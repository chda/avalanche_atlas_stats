# -*- coding: utf-8 -*-
"""
Avalanche_DB connection examples. 
Query in style and comfort.
Created on Tue Nov 12 16:58:32 2019

Relies on pandas, geopandas, shapely, psycopg2, getpass modules.

@author: Christian Demmler
"""

import avaconnect
# Will return a geodataframe with all events after 1970 on northerly slopes. Optional Boolean argument: Geometry data only?
data = avaconnect.eventlist("WHERE (origin_exposition_text = '{}') AND (extract(year FROM timestamp) >= {})".format('N',1970), True )

# Will return a reduced geodataframe with all events happening on avalanche paths starting with 'Ahorn'
data_onlygeom = avaconnect.eventlist("WHERE path_name ilike 'Ahorn%'", True)

# Will return only event point and line geometry (3d = True) for northern, northwesterly and northeasterly avalanches
data_minimumgeom = avaconnect.event_minimum("where (origin_exposition_text like 'N%')", True)

# Will query the paths table directly. Free Sql input, have fun. Will return a straight Pandas Dataframe with the results. 
data_freequery = avaconnect.query('select * from paths')