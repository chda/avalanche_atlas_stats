# -*- coding: utf-8 -*-
"""
Created on Tue Nov 12 16:35:05 2019
Connection to avalanche-db postgresql server running on 
postgres:[pwd]@194.37.53.124:5432/avalanche_db

query is the base function, other functions provide simple ways to do common queries.
@author: Christian Demmler
"""

import psycopg2

import pandas.io.sql as sqlio
import shapely
from shapely import wkt, wkb
import geopandas
#import getpass
import gdal

def query(query_list: object = 'SELECT * FROM event_full', constraint: object = '') -> object:
    try:
        if(query_list.lower().find('from')!=-1):
        # Set up our connection
                 
            server_ip = '194.37.53.124'
            db_name = 'avalanche_db'
            username = 'postgres'
            pwd = 'bfw'
            # ...and connect please
            conn= psycopg2.connect("host='{}' port={} dbname='{}' user={} password={}".format(server_ip, 5432, db_name, username, pwd))
            sql = '{} {};'.format(query_list.strip(),constraint.strip())
            dat = sqlio.read_sql_query(sql, conn)
            conn = None
            return dat
       
        else:
            print('Something wrong with the query. No "select" and/or "from" encountered.')
    except:
        print('Error: Connection error.')
        raise

# Connect to an existing database
def eventlist(constraint='', geom_only = False): 
    
    if (geom_only):
            sql = 'SELECT * FROM event_geom'
    else:
            sql ='"SELECT * FROM event_full'
    dat = query(sql, constraint)
        #drop connection after use
    
        #use well-known-text geometry, create designated geometry column for geopandas  
    dat['geom']=dat['pt'].apply(wkt.loads)
    
    if dat is not None:
        geodat = geopandas.GeoDataFrame(dat, geometry='geom')
    return geodat

def event_minimum(constraint='', use3d=True):
    # will return only event point and path line geometries (either 3d oder 2d) from events as selected by the constraint list
    if (use3d):
        query_list = 'SELECT pt3d, path_ln3d FROM event_full'
        geom_col = 'pt3d'
    else:
        query_list = 'SELECT pt, path_ln FROM event_full'
        geom_col= 'pt'
    dat = query(query_list, constraint)
    
    
    dat['geom']=dat[geom_col].apply(wkt.loads)
    if dat is not None:
        geodat = geopandas.GeoDataFrame(dat, geometry='geom')
    return geodat

def get_point(command = "SELECT * from events_furthest where path_id = 18", field = 'geom_event_pt3d'):
    #Example for pulling a simple Point geometry as xyz array. Takes an existing field, geom_event_pt3d from a view. 
    list=query(command)
    points  = []
    for point in list[field]:
        points.append(shapely.wkb.loads(point, hex = True))
    #print('Imported:')
    return (points[0].coords[:]) #prints first and only geometry of first point


def get_line(command = 'SELECT profile_from_peak(geom_path_ln3d) as dz from event_full WHERE event_id = 41', field='dz'):
    #Example for pulling a Linestring geometry as xyz-multiline array. Uses a function, profile_from_peak, to generate the profile from a path line, from a view. 
    list=query(command)
    paths  = []
    for path in list[field]:
        paths.append(shapely.wkb.loads(path, hex = True))
    #print('Imported:')
    print(paths[0].coords[:])
    return (paths[0].coords[:]) #prints first and only geometry of first point


def get_raster(path_id, rastercol = 'dem'):
    #available raster columns: dem, rel_rast (release area), dep_rast (deposition area)
    data = query( "select st_transform(geom_path_ln, st_srid(reference.rast)) as path, "\
            "st_astiff(reference.rast) as dem, "\
            "st_astiff(st_asraster(st_makepolygon(st_addpoint(geom_rel_event_ln,st_startpoint(geom_rel_event_ln))), reference.rast)) as rel_rast, "\
            "st_astiff(st_asraster(st_makepolygon(st_addpoint(geom_event_ln,st_startpoint(geom_event_ln))), reference.rast)) as dep_rast, "\
            "st_srid(reference.rast) as srid from event_full,"\
            "(SELECT path_id, rast from path_dems WHERE path_id = {}) as reference "\
            "where event_full.path_id = {}".format(path_id, path_id))
    vsipath = '/vsimem/from_postgis'
    gdal.FileFromMemBuffer(vsipath, data['dem'][0].tobytes())
    
    ds = gdal.Open(vsipath)
    
    band = ds.GetRasterBand(1)
    arr = band.ReadAsArray()
    ds = band = None
    return arr

#Command for finding all sections of forest as multilines:
forestquery = "WITH aforest AS (SELECT forest.rast FROM forest, active_rasters WHERE forest.filename = active_rasters.forest), "\
     "const AS (SELECT st_srid(aforest.rast) AS rast_srid FROM aforest LIMIT 1),"\
     "vect AS (SELECT st_srid(paths_1.ln) AS vect_srid FROM paths paths_1 LIMIT 1),"\
     "polydump AS (SELECT st_dumpaspolygons(st_clip(aforest.rast, st_buffer(st_transform(st_convexhull(paths_1.ln), const_1.rast_srid), 10.0::double precision), 0::double precision)) AS dump FROM paths paths_1  CROSS JOIN const const_1 LEFT JOIN aforest ON st_intersects(aforest.rast, st_transform(paths_1.ln, const_1.rast_srid)))"\
     "SELECT paths.path_id, st_collectionhomogenize(st_collect(st_intersection(paths.ln, st_buffer(st_transform((polydump.dump).geom, vect.vect_srid), 0.0001::double precision)))) AS forest_path FROM const CROSS JOIN vect CROSS JOIN paths LEFT JOIN polydump ON st_intersects(paths.ln, st_transform((polydump.dump).geom, vect.vect_srid)) GROUP BY paths.path_id;"
#get_raster()
