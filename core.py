"""
The core of this model will include the parts of the model that are not unique to a process.
1. Call in data         Responsible: Michael & Christopher
3. Call process type    Responsible: Avalanche-Christopher & Michael, Rockfall- Jean-baptiste
4. Plot results 1D      Responsible: Christopher
Database connection: Christian Demmler
"""

import sys

from numpy.core._multiarray_umath import ndarray

import read_data as rd

import avaconnect
import ResultsDisplay
import numpy as np
import shapely
import matplotlib.pyplot as plt
from scipy import interpolate


def regrid(resolution, profile):
    '''
    Interpolation of Points along the Profile with a certain resolution
    '''
    x = np.arange(0, profile[-1, 0], resolution)
    #inter = interpolate.interp1d(profile[: , 0], profile[: , 1])
    inter = interpolate.interp1d(profile[: , 0], profile[: , 1], axis=0, fill_value="extrapolate")
    z_new = inter(x)
    profile_resolution = np.column_stack((x, z_new))
    return profile_resolution

def avalanch_db_data(path_id):
    events = avaconnect.query(  "select path_id, "
                                "avalanche_size, "
                                "path_name, "
                                "st_astext("
                                    "st_startpoint("
                                        "profile_from_peak("
                                            "geom_origin_pt, "
                                            "add_interpolate_point("
                                               "add_interpolate_point("
                                                    "resampled_ln_z("
                                                        "geom_path_ln"
                                                        "),"
                                                        "geom_origin_pt3d"
                                                    "),"
                                                    "geom_runout_pt3d"
                                                ")"
                                            ")"
                                        ")"
                                    ") as origin, "
                                "st_astext("
                                    "st_startpoint("
                                        "profile_from_peak("
                                            "geom_transit_pt, "
                                            "add_interpolate_point("
                                                "add_interpolate_point("
                                                    "resampled_ln_z("
                                                        "geom_path_ln"
                                                    "),"
                                                    "geom_origin_pt3d),"
                                                    "geom_runout_pt3d"
                                                ")"
                                            ")"
                                        ")"
                                    ") "
                                    "as transit, "
                                    "st_astext("
                                        "st_startpoint("
                                            "profile_from_peak("
                                                "geom_runout_pt, "
                                                "add_interpolate_point("
                                                    "add_interpolate_point("
                                                        "resampled_ln_z("
                                                            "geom_path_ln"
                                                        "),"
                                                        "geom_origin_pt3d"
                                                    "),"
                                                    "geom_runout_pt3d"
                                                ")"
                                            ")"
                                        ")"
                                    ") "
                                "as runout  from event_full where path_id = {}".format(path_id))
    return  events


def avalanche_db_events(path_id):
    querystr = "SELECT "\
            "(st_dumppoints(profile_from_peak(geom_event_pt, add_interpolate_point( add_interpolate_point(resampled_ln_z(geom_path_ln), geom_origin_pt3d), geom_runout_pt3d)))).geom, "\
            "st_z(geom_event_pt3d), "\
            "geom_event_pt3d, "\
            "st_astext(profile_from_peak(ST_3DClosestPoint(geom_event_pt3d, resampled_ln_z(geom_path_ln3d)), resampled_ln_z(geom_path_ln))) as event_snap, "\
            "avalanche_size, "\
            "timestamp "\
        "FROM "\
            "event_full "\
        "WHERE "\
            "path_id = {}".format(path_id)
    events = avaconnect.query( querystr)
    print(path_id)
    return events


def avalanche_db_path(profile_id):
    querystr=\
    "SELECT" \
        "distinct profile_from_peak(add_interpolate_point(add_interpolate_point(resampled_ln_z(geom_path_ln),"\
        "geom_origin_pt3d,"\
        "geom_runout_pt3d) )as dz"\
    "FROM" \
        "event_full" \
    "WHERE" \
        "path_id = {}".format(profile_id     )
    profile = np.array(avaconnect.get_line(querystr, 'dz'))
    profile_res = regrid(1, profile)
    return profile_res


######################### core #################################################
paths = rd.read_db_available_paths()

fig, ax = plt.subplots(figsize=(10, 5)) # make fig and axis before loop
# Reading in data
#for path in paths.iterrows(): # this is a loop over all paths.
path_id = 35 #path[1]['path_id']
#ava_data = avalanch_db_data(path_id)
#profile = avalanche_db_path(path_id)
events = avalanche_db_events(path_id)
origin = ava_data['origin'][0]
xy_crown = shapely.wkt.loads(origin).coords[0]     #              x converter
event_pts =[]
for line in events:
    event_pts.append.shapely.wkt.loads(line['pt3d']).coords[0]
#xy_event=[]
####################### import avalanche release point ####################
if ava_data['origin'][0] is not None:
    start_pt = int(shapely.wkt.loads(ava_data['origin'][0]).coords[0][0])
else:
    start_pt = 0
    pritn("no start point on path ")
######################## loop over event points make alpha lines and draw them! #########
for event in events['st_z']: #range(0,len(events[:,0]),1):
    #xy_event.append(shapely.wkt.loads(events[1][i])) # xyz of events
    print("event ",event)
    #xy_event = shapely.wkt.loads(event[1])







########################## plot ###############################################


ax.plot(profile[:, 0], profile[:, 1], label='Profile', color='black')

origin = shapely.wkt.loads(ava_data['origin'][0])
start_deposit = shapely.wkt.loads(ava_data['runout'][0])
ax.scatter(origin.coords[0][0], origin.coords[0][1], color='tab:blue', label='Start')
ax.scatter(start_deposit.coords[0][0], start_deposit.coords[0][1], color='tab:red', label='Expert')
for event in events:
    alpha = np.arctan(())
    #point = shapely.wkt.loads(event[0], hex=True)
    #col = [random.randrange(0,255)/255, random.randrange(0, 255)/255, random.randrange(0, 255)/255] # hacking color scale
    #ax.scatter(point.coords[0][0], point.coords[0][1], color=col, label=event[2].year, marker='o', s=80)
ax.set_xlabel("Distance [m]")
ax.set_ylabel('Altitude [m]')

fig.legend(loc='lower left', bbox_to_anchor=(0.08, 0.12), ncol=3)
fig.tight_layout()
plt.show()

